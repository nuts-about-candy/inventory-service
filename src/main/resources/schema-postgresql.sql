CREATE TABLE IF NOT EXISTS CATEGORY (
 id bigserial not null primary key,
 name varchar(100) not null
);

CREATE TABLE IF NOT EXISTS PRODUCT (
 id bigserial not null primary key,
 name varchar(100) not null,
 description varchar(255) ,
 stock integer,
 classification_id integer,
 category_id integer
);

CREATE TABLE IF NOT EXISTS PRODUCT_CLASSIFICATION (
 id bigserial not null primary key,
 name varchar(20)
);

CREATE TABLE IF NOT EXISTS PRODUCT_SIZE (
 id bigserial NOT NULL PRIMARY KEY,
 size varchar(10) NOT NULL,
 label varchar(30) NOT NULL,
 prize real NOT NULL,
 weight integer not null,
 prod_class_id integer not null
)