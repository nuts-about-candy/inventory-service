package com.nutsaboutcandy.inventory;


import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.nutsaboutcandy.inventory.jwt.JWTAuthorizationFilter;
import com.nutsaboutcandy.inventory.jwt.JWTConfig;


@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{

	private final JWTConfig config;
	
	@Autowired
	public ApplicationSecurityConfig(JWTConfig config) {
		
		this.config = config;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
		.cors()
		.and()
		.csrf()
			.disable()
		.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.addFilterAfter(new JWTAuthorizationFilter(config), UsernamePasswordAuthenticationFilter.class)
		.authorizeRequests()
			.antMatchers("/inventory*", "/inventory/**").permitAll()//.hasAnyAuthority("ADMIN")
			.antMatchers("/categories*").permitAll()
			.antMatchers("/product-class*").permitAll()
		.anyRequest()
		.authenticated();
	}
	
	@Bean
    CorsConfigurationSource corsConfigurationSource() {
		
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "DELETE", "PUT"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
