package com.nutsaboutcandy.inventory.msg.queue;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutsaboutcandy.inventory.model.ProductsWeights;
import com.nutsaboutcandy.inventory.service.InventoryService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

@Component
public class MsgQueueListener implements ChannelAwareMessageListener {

	@Autowired
	private InventoryService invService;
	
	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		ProductsWeights weights = new ObjectMapper()
				.readValue(message.getBody(), ProductsWeights.class);

		
		String response = "OK";
		try {
			invService.hasQuantity(weights);
		}catch(Exception e) {
			response = "ERROR";
		}
		System.err.println("CHECKOUT: " + weights + ": " + response );
		
		MessageProperties messageProperties = message.getMessageProperties();
		AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
				.correlationId(messageProperties.getCorrelationId()).build();
		System.err.println(messageProperties.getCorrelationId());
		
		channel.basicPublish("", messageProperties.getReplyTo(), replyProps, response.getBytes("UTF-8"));
//		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
	}
}
