package com.nutsaboutcandy.inventory.entity;

import java.util.List;

public class ProductResult {

	private int count;
	private List<Product> products;
	
	public ProductResult(int count, List<Product> products) {
		super();
		this.count = count;
		this.products = products;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
}
