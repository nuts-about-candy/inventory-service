package com.nutsaboutcandy.inventory.entity;

import java.util.List;

public class Product {

	private long id;
	private String name;
	private String description;
	private long stock;
	private long categoryId;
	private long classificationId;
	private String classification;
	private String category;
	private List<ProductSize> productSizes;
	
	public Product() {}
	
	public Product(String name, String description, long categoryId, long stock, long classificationId) {
		super();
		this.name = name;
		this.description = description;
		this.categoryId = categoryId;
		this.stock = stock;
		this.classificationId = classificationId;
	}

	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getStock() {
		return stock;
	}
	public void setStock(long stock) {
		this.stock = stock;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(long classificationId) {
		this.classificationId = classificationId;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<ProductSize> getProductSizes() {
		return productSizes;
	}

	public void setProductSizes(List<ProductSize> productSizes) {
		this.productSizes = productSizes;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", stock=" + stock
				+ ", categoryId=" + categoryId + ", classificationId=" + classificationId + ", classification="
				+ classification + ", category=" + category + ", productSizes=" + productSizes + "]";
	}
	
	
}
