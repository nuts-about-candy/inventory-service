package com.nutsaboutcandy.inventory.entity;

public class ProductSize {

	private long id;
	private String size;
	private String label;
	private double prize;
	private long weight;
	private long prodClassId;
	
	public ProductSize() {}
	
	public ProductSize(String size, String label, double prize, long weight) {
		this.size = size;
		this.label = label;
		this.prize = prize;
		this.weight = weight;
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public long getProdClassId() {
		return prodClassId;
	}
	public void setProdClassId(long prodClassId) {
		this.prodClassId = prodClassId;
	}

	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "ProductSize [id=" + id + ", size=" + size + ", label=" + label + ", prize=" + prize + ", weight="
				+ weight + ", prodClassId=" + prodClassId + "]";
	}

	
}


