package com.nutsaboutcandy.inventory.entity;

import java.util.List;

public class ProductClassification {

	private long id;
	private String name;
	private List<ProductSize> productSizes;
	
	public ProductClassification() {}
	
	public ProductClassification(String name, List<ProductSize> productSizes) {
		this.name = name;
		this.productSizes = productSizes;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<ProductSize> getProductSizes() {
		return productSizes;
	}

	public void setProductSizes(List<ProductSize> productSizes) {
		this.productSizes = productSizes;
	}

	@Override
	public String toString() {
		return "ProductClassification [id=" + id + ", name=" + name + ", productSizes=" + productSizes + "]";
	}

}
