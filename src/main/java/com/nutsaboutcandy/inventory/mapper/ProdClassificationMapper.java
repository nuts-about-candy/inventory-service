package com.nutsaboutcandy.inventory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nutsaboutcandy.inventory.entity.ProductClassification;
import com.nutsaboutcandy.inventory.entity.ProductSize;

@Mapper
public interface ProdClassificationMapper {

	@Select("SELECT * FROM PRODUCT_CLASSIFICATION order by name")
	@Results({
		@Result(column = "name", property = "name"),
	})
	List<ProductClassification> findAll();
	
	@Select("SELECT * FROM PRODUCT_CLASSIFICATION "
			+ "WHERE id = #{id}  ORDER BY name")
	@Results({
		@Result(column = "name", property = "name"),
	})
	ProductClassification findById(long id);
	
	@Select("SELECT * FROM PRODUCT_SIZE WHERE prod_class_id = #{id}")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "size", property = "size"),
		@Result(column = "label", property = "label"),
		@Result(column = "weight", property = "weight"),
		@Result(column = "prize", property = "prize")
	})
	List<ProductSize> findProductSizes(long id);
	
	@Select("INSERT INTO PRODUCT_CLASSIFICATION (name) VALUES "
			+ "(#{name}) RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	long insert(ProductClassification prodClass);
	
	@Select("INSERT INTO PRODUCT_SIZE (size, label, prize, weight, prod_class_id) VALUES "
			+ "(#{size}, #{label}, #{prize}, #{weight}, #{prodClassId}) RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	long insertProductSize(ProductSize productSize);

	
	
}
