package com.nutsaboutcandy.inventory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.nutsaboutcandy.inventory.entity.Product;

@Mapper
public interface InventoryMapper {

	@Select("INSERT INTO PRODUCT (name, description, stock, classification_id, category_id) VALUES "
			+ "(#{name}, #{description}, #{stock}, #{classificationId}, #{categoryId}) "
			+ "RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insert(Product product);
	
	@Update("UPDATE PRODUCT set name = #{name}, description = #{description}, "
			+ "classification_id = #{classificationId}, category_id = #{categoryId} "
			+ "WHERE id = #{id} ")
	void update(Product product);
	
	@Select("SELECT *, C.name as category, PC.name as classification "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "ORDER BY C.name, PC.name, P.name "
			+ "OFFSET #{offset} LIMIT #{limit}")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
		@Result(column = "description", property = "description"),
		@Result(column = "stock", property = "stock"),
		@Result(column = "category_id", property = "categoryId"),
		@Result(column = "classification_id", property = "classificationId"),
		@Result(column = "classification", property = "classification"),
		@Result(column = "category", property = "category")
	})
	List<Product> findAll(int limit, int offset);
	
	@Select("SELECT count(P.id) FROM PRODUCT P "
			+"JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id")
	@Results({
		@Result(column = "count", property = "count")
	})
	int countAllProducts();

	@Select("SELECT *, C.name as category, PC.name as classification "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "WHERE P.id = #{id}")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
		@Result(column = "description", property = "description"),
		@Result(column = "stock", property = "stock"),
		@Result(column = "category_id", property = "categoryId"),
		@Result(column = "classification_id", property = "classificationId"),
		@Result(column = "classification", property = "classification"),
		@Result(column = "category", property = "category")
	})
	Product findByProductId(long id);
	
	@Select("SELECT *, C.name as category, PC.name as classification "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "WHERE category_id = #{id} "
			+ "ORDER BY PC.name, P.name")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
		@Result(column = "description", property = "description"),
		@Result(column = "stock", property = "stock"),
		@Result(column = "category_id", property = "categoryId"),
		@Result(column = "classification_id", property = "classificationId"),
		@Result(column = "classification", property = "classification"),
		@Result(column = "category", property = "category")
	})
	List<Product> findByCategoryId(long id);
	
	@Select("SELECT count(P.id) "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "WHERE category_id = #{id} ")
	@Results({
		@Result(column = "count", property = "count")
	})
	int countByCategoryId(long id);

	@Select("SELECT *, C.name as category, PC.name as classification "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "WHERE category_id = #{id} "
			+ "ORDER BY PC.name, P.name")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
		@Result(column = "description", property = "description"),
		@Result(column = "stock", property = "stock"),
		@Result(column = "category_id", property = "categoryId"),
		@Result(column = "classification_id", property = "classificationId"),
		@Result(column = "classification", property = "classification"),
		@Result(column = "category", property = "category"),
	})
	List<Product> findForSaleProductByCategory(long id);
	
	@Select("SELECT *, C.name as category, PC.name as classification "
			+ "FROM PRODUCT P "
			+ "JOIN CATEGORY C ON C.id=P.category_id "
			+ "JOIN PRODUCT_CLASSIFICATION PC ON PC.id=P.classification_id "
			+ "WHERE P.id = #{id} "
			+ "ORDER BY PC.name, P.name")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
		@Result(column = "description", property = "description"),
		@Result(column = "stock", property = "stock"),
		@Result(column = "category_id", property = "categoryId"),
		@Result(column = "classification_id", property = "classificationId"),
		@Result(column = "classification", property = "classification"),
		@Result(column = "category", property = "category"),
	})
	Product findForSaleProductByProductId(long id);
	
	@Delete("DELETE FROM PRODUCT WHERE id = #{id}")
	void delete(long id);

	@Update("UPDATE PRODUCT SET stock = #{stock}")
	void updateStock(Product prod);

	@Update("UPDATE PRODUCT SET stock = #{value} WHERE id = #{id}")
	void updateStockById(long id, int value);

	

}
