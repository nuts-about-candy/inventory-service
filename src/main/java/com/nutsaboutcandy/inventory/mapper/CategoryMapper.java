package com.nutsaboutcandy.inventory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nutsaboutcandy.inventory.entity.Category;


@Mapper
public interface CategoryMapper {

	@Select("INSERT INTO CATEGORY (name) VALUES (#{name}) RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insert(Category category);
	
	@Select("SELECT * FROM CATEGORY ORDER BY name")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name"),
	})
	List<Category> findAll();

	@Delete("DELETE FROM CATEGORY WHERE id = #{id}")
	void delete(long id);
}
