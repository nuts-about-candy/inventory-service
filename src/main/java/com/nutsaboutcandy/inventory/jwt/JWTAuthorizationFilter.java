package com.nutsaboutcandy.inventory.jwt;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

	private final JWTConfig config;
	
	public JWTAuthorizationFilter(JWTConfig config) {
		this.config = config;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, 
			FilterChain filterChain)
			throws ServletException, IOException {

		String authorizationHeader = request.getHeader(config.getAuthorization());
		
		if(Objects.isNull(authorizationHeader) ||
				!authorizationHeader.startsWith(config.getTokenPrefix())) {
			filterChain.doFilter(request, response);
			return;
		}
		
		try {
			String token = authorizationHeader.replace(config.getTokenPrefix(), "");
			
			Jws<Claims> claimsJws = Jwts.parser()
				.setSigningKey(config.getSecretKeyForSigning())
				.parseClaimsJws(token);
				
			Claims body = claimsJws.getBody();
			String username = body.getSubject();
			List<Map<String, String>>  authorities = (List<Map<String, String>>) body.get(config.getAuthorizatiesKey());
			
			Set<SimpleGrantedAuthority> grantedAuthorities = authorities.stream()
					.map(m -> new SimpleGrantedAuthority(m.get("authority")))
					.collect(Collectors.toSet());
			
			Authentication authentication = new UsernamePasswordAuthenticationToken(
					username,
					null,
					grantedAuthorities
			);
			SecurityContextHolder.getContext().setAuthentication(authentication );
			
		} catch(JwtException e) {
			throw new IllegalStateException("Token cannot be trusted.");
		}
		
		filterChain.doFilter(request, response);
		
	}

}
