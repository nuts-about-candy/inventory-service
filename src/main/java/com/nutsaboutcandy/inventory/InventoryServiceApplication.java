package com.nutsaboutcandy.inventory;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nutsaboutcandy.inventory.entity.Category;
import com.nutsaboutcandy.inventory.entity.ProductClassification;
import com.nutsaboutcandy.inventory.entity.ProductSize;
import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.service.CategoryService;
import com.nutsaboutcandy.inventory.service.InventoryService;
import com.nutsaboutcandy.inventory.service.ProdClassificationService;

@SpringBootApplication
public class InventoryServiceApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(InventoryServiceApplication.class, args);
	}

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private ProdClassificationService prodClassService;
	
	
	@Override
	public void run(String... args) throws Exception {
		
		// DATA WILL BE INSERTED IF TABLES ARE EMPTY
		insertData();
	}

	private void insertData() {
		
		// CHECK IF CATEGORY TABLE HAS DATA
		if(categoryService.getAllCategory().size() == 0) {
			long cat1 = categoryService.addCategory(new Category("All about Nuts"));
			long cat2 = categoryService.addCategory(new Category("All about Candies"));
			long cat3 = categoryService.addCategory(new Category("Nuts and Candies"));
			
			// CHECK IF PRODUCT CLASSIFICATION TABLE HAS DATA
			if(prodClassService.getAllProdClassification().size() == 0) { 
			
				List<ProductSize> sizesPremium = Arrays.asList(
						new ProductSize("small", "Small (50gm)", 70.0, 50),
						new ProductSize("medium", "Medium (100gm)", 120.0, 100),
						new ProductSize("large", "Large (150gm)", 170.0, 150));
				List<ProductSize> sizesRegular = Arrays.asList(
						new ProductSize("small", "Small (50gm)", 50.0, 50),
						new ProductSize("medium", "Medium (100gm)", 100.0, 100),
						new ProductSize("large", "Large (150gm)", 150.0, 150));
				
				long class1 = prodClassService.addProdClassification(
						new ProductClassification("PREMIUM", sizesPremium));
				long class2 = prodClassService.addProdClassification(
						new ProductClassification("REGULAR", sizesRegular));
				
				// CHECK IF INVENTORY TABLE HAS DATA
				if(inventoryService.getAllProducts(1, 1).size() == 0) {
					
					inventoryService.addProduct(
							new Product("Kings of Nuts", "(Macadamia, Pistachios and Cashews)", 
								cat1, 1000, class1));
						inventoryService.addProduct(
							new Product("Mixed Nut Brittle", "(Peanuts, almonds and cashews covered in a sweet and salty caramel brittle)", 
								cat1, 1000, class1));
						inventoryService.addProduct(
							new Product("Cheesy Mac", "(Parmesan Cheese Macadamia Nuts)", 
								cat1, 1000, class1));
						inventoryService.addProduct(
							new Product("Bar Nuts", "(Spicy sweet peanuts, almonds, walnuts, brazil nuts)", 
								cat1, 1000, class2));
						inventoryService.addProduct(
							new Product("Crazy Samurai", "(Japanese rice crackers, soy nuts and wasabi peas)", 
								cat1, 1000, class2));
						inventoryService.addProduct(
							new Product("Cathay", "(batter-coated peanuts and soy nuts, barbecued almonds, sesame sticks, pumpkin seeds and roasted peanuts)", 
								cat1, 1000, class2));
						inventoryService.addProduct(
							new Product("Garlic Peanuts", "", 
								cat1, 1000, class2));
						
						inventoryService.addProduct(
							new Product("Salt water taffey", "", 
								cat2, 1000, class1));
						inventoryService.addProduct(
							new Product("Caramels", "", 
								cat2, 1000, class1));
						inventoryService.addProduct(
							new Product("English Toffee", "", 
								cat2, 1000, class1));
						inventoryService.addProduct(
							new Product("Nougat Whirls", "(Caramel, peanut and chocolate nougat swirls)", 
								cat2, 1000, class1));
						inventoryService.addProduct(
							new Product("Sour Balls", "", 
								cat2, 1000, class2));
						inventoryService.addProduct(
							new Product("Jelly Beans", "", 
								cat2, 1000, class2));
						inventoryService.addProduct(
							new Product("Jumbo Jellies", "(Rainbow colored gumdrops)", 
								cat2, 1000, class2));
						
						inventoryService.addProduct(
							new Product("Fruit and Nut", "(Roasted almonds with Chocolate Raisins)", 
								cat3, 1000, class1));
						inventoryService.addProduct(
							new Product("Rave Dark", "(Roasted almonds, cashews, and hazel nuts covered in dark belgian chocolate)", 
								cat3, 1000, class1));
						inventoryService.addProduct(
							new Product("Rave Milk", "(Roasted almonds, cashews, and hazel nuts covered in belgian milk chocolate)", 
								cat3, 1000, class1));
						inventoryService.addProduct(
							new Product("Almond Roca", "", 
								cat3, 1000, class1));
						inventoryService.addProduct(
							new Product("Rainbow Peanuts", "(Peanut wrapped in chocolate in a crunchy candy shell)", 
								cat3, 1000, class2));
						inventoryService.addProduct(
							new Product("Tropical", "(Pineapple chunks, papaya wedges, coconut, sweet banana slices, brazil nuts and rich toasted cashews)", 
								cat3, 1000, class2));
				}
			}
		}
	}

}
