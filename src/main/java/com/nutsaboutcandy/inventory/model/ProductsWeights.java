package com.nutsaboutcandy.inventory.model;

import java.util.Map;

public class ProductsWeights {

	private Map<Long, Integer> weights;

	public Map<Long, Integer> getWeights() {
		return weights;
	}

	public void setWeights(Map<Long, Integer> weights) {
		this.weights = weights;
	}

	@Override
	public String toString() {
		return "ProductsWeights [weights=" + weights + "]";
	}

	
}
