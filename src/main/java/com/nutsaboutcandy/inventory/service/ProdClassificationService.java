package com.nutsaboutcandy.inventory.service;

import java.util.List;

import com.nutsaboutcandy.inventory.entity.ProductClassification;

public interface ProdClassificationService {
	
	List<ProductClassification> getAllProdClassification();
	
	ProductClassification getProdClassById(long id);
	
	long addProdClassification(ProductClassification prodClass);

}
