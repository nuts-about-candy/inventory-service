package com.nutsaboutcandy.inventory.service;

import java.util.List;

import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.model.ProductsWeights;

public interface InventoryService {

	long addProduct(Product product);
	
	List<Product> getAllProducts(int pageSize, int page);
	
	Product getProductByProductId(long id);
	
	List<Product> getProductsByCategoryId(long id);
	
	void deleteProduct(long id);

	void resetStocks();

	void add1kgToStocks();

	void editProduct(Product product);

	List<Product> getProductsForSaleByCategory(long id);

	Product getProductForSaleByProductId(long id);

	void hasQuantity(ProductsWeights weights);

	int countAllProducts();

	int countProductsByCategory(long id);
}
