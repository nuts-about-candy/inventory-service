package com.nutsaboutcandy.inventory.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutsaboutcandy.inventory.entity.Category;
import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.mapper.CategoryMapper;
import com.nutsaboutcandy.inventory.mapper.InventoryMapper;
import com.nutsaboutcandy.inventory.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryMapper categoryMapper;
	
	@Autowired
	private InventoryMapper invMapper;
	
	@Override
	public List<Category> getAllCategory() {
		return categoryMapper.findAll();
	}

	@Override
	public long addCategory(Category category) {
		return categoryMapper.insert(category);
	}

	@Override
	public void deleteCategory(long id) {
		categoryMapper.delete(id);
		
		// GET ALL PRODUCT IN CATEGORY AND DELETE
		List<Product> products = invMapper.findByCategoryId(id);
		products.stream().forEach(prod -> invMapper.delete(prod.getId()));
	}

	
}
