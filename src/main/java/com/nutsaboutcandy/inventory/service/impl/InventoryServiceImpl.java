package com.nutsaboutcandy.inventory.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.mapper.InventoryMapper;
import com.nutsaboutcandy.inventory.mapper.ProdClassificationMapper;
import com.nutsaboutcandy.inventory.model.ProductsWeights;
import com.nutsaboutcandy.inventory.service.InventoryService;

@Service
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	private InventoryMapper inventoryMapper;
	
	@Autowired
	private ProdClassificationMapper prodClassMapper;
	
	@Override
	public long addProduct(Product product) {
		
		product.setStock(1000);
		return inventoryMapper.insert(product);
	}

	@Override
	public void editProduct(Product product) {
		inventoryMapper.update(product);
	}
	
	@Override
	public List<Product> getAllProducts(int pageSize, int page) {

		int offset = pageSize * (page - 1);
		List<Product> list = inventoryMapper.findAll(pageSize, offset);
		list.stream().forEach(prod -> {
			prod.setProductSizes(prodClassMapper.findProductSizes(prod.getClassificationId()));
		});
		return list;
	}

	@Override
	public int countAllProducts() {
		return inventoryMapper.countAllProducts();
	}

	@Override
	public Product getProductByProductId(long id) {
		return inventoryMapper.findByProductId(id);
	}

	@Override
	public List<Product> getProductsByCategoryId(long id) {
		return inventoryMapper.findByCategoryId(id);
	}
	
	@Override
	public int countProductsByCategory(long id) {
		return inventoryMapper.countByCategoryId(id);
	}

	@Override
	public List<Product> getProductsForSaleByCategory(long id) {
		List<Product> list = inventoryMapper.findForSaleProductByCategory(id);
		list.stream().forEach(prod -> {
			prod.setProductSizes(prodClassMapper.findProductSizes(prod.getClassificationId()));
		});
		return list; 
	}
	@Override
	public Product getProductForSaleByProductId(long id) {
		
		Product prod = inventoryMapper.findForSaleProductByProductId(id);
		if(prod != null) {
			prod.setProductSizes(prodClassMapper.findProductSizes(prod.getClassificationId()));
			return prod;
		}
		return null;
	}

	@Override
	public void deleteProduct(long id) {
		inventoryMapper.delete(id);
	}

//	@Override
//	public void resetStocks() {
//		List<Product> products = inventoryMapper.findAll();
//		products.stream().forEach(prod -> {
//			prod.setStock(1000);
//			inventoryMapper.updateStock(prod);
//		});
//	}
//
//	@Override
//	public void add1kgToStocks() {
//
//		List<Product> products = inventoryMapper.findAll();
//		products.stream().forEach(prod -> {
//			long stock = prod.getStock() + 1000;
//			prod.setStock(stock);
//			inventoryMapper.updateStock(prod);
//		});
//	}

	@Override
	public void hasQuantity(ProductsWeights weights) {
		
			
			weights.getWeights().entrySet()
				.stream().forEach(e -> {
					Product prod = inventoryMapper.findByProductId(e.getKey());
					if(e.getValue() > prod.getStock())
						throw new RuntimeException(e.getKey().toString());
					else {
						int newStock = (int) (prod.getStock() - e.getValue());
						e.setValue(newStock);
					}
				});
			
//			weights.getWeights().entrySet()
//				.stream()
//				.forEach(e -> inventoryMapper.updateStockById(e.getKey(), e.getValue()));
	}

	@Override
	public void resetStocks() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add1kgToStocks() {
		// TODO Auto-generated method stub
		
	}

}
