package com.nutsaboutcandy.inventory.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutsaboutcandy.inventory.entity.ProductClassification;
import com.nutsaboutcandy.inventory.mapper.ProdClassificationMapper;
import com.nutsaboutcandy.inventory.service.ProdClassificationService;

@Service
public class ProdClassificationServiceImpl implements ProdClassificationService{

	@Autowired
	private ProdClassificationMapper prodClassMapper;
	
	@Override
	public List<ProductClassification> getAllProdClassification() {
		
		List<ProductClassification> list = prodClassMapper.findAll();
		list.stream().forEach(prodClass -> {
			prodClass.setProductSizes(prodClassMapper.findProductSizes(prodClass.getId()));
		});
		return list;
	}

	@Override
	public ProductClassification getProdClassById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long addProdClassification(ProductClassification prodClass) {
		
		long id = prodClassMapper.insert(prodClass);
		prodClass.getProductSizes().stream()
			.forEach(size -> {
				size.setProdClassId(id);
				prodClassMapper.insertProductSize(size);
			});
		
		return id;
	}

}
