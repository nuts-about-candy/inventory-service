package com.nutsaboutcandy.inventory.service;

import java.util.List;

import com.nutsaboutcandy.inventory.entity.Category;

public interface CategoryService {

	List<Category> getAllCategory();
	
	long addCategory(Category category);
	
	void deleteCategory(long id);
}
