package com.nutsaboutcandy.inventory.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.entity.ProductResult;
import com.nutsaboutcandy.inventory.service.InventoryService;


@RestController
@RequestMapping("/inventory/products")
public class ProductController {
	
	@Autowired
	private InventoryService invService;

	@GetMapping("/category/{id}")
	public ResponseEntity<Object> getProductsForSaleByCategory(@PathVariable("id") long id) {
		
		List<Product> products = invService.getProductsForSaleByCategory(id);
		int count = invService.countProductsByCategory(id);
		ProductResult result = new ProductResult(count, products);
		return ResponseEntity.ok().body(result);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getProductForSaleByProductId(@PathVariable("id") long id) {
		
		Product prod = invService.getProductForSaleByProductId(id);
		if(prod != null)
			return ResponseEntity.ok().body(prod);
		
		return new ResponseEntity<>(
				"{\"message\":\"Product does not exist.\"}", HttpStatus.NOT_FOUND);
		
	}
	
	@GetMapping
	public ResponseEntity<Object> getAllProducts(@RequestParam("pageSize") int pageSize,
			@RequestParam("page") int page) {
		
		List<Product> products = invService.getAllProducts(pageSize, page);
		int count = invService.countAllProducts();
		ProductResult result = new ProductResult(count, products);
		return ResponseEntity.ok().body(result);
	}
}
