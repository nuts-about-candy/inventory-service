package com.nutsaboutcandy.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.inventory.entity.Category;
import com.nutsaboutcandy.inventory.service.CategoryService;

@RestController
@RequestMapping("/inventory/categories")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;

	@GetMapping
	public ResponseEntity<Object> getAllCategories() {
		
		return ResponseEntity.ok().body(categoryService.getAllCategory());
	}
	
	@PostMapping
	public ResponseEntity<Object> addCategory(@RequestBody Category category) {
		
		long id = categoryService.addCategory(category);
		category.setId(id);
		return ResponseEntity.ok().body(category);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") long id) {
		
		categoryService.deleteCategory(id);
		
		return ResponseEntity.ok().build();
	}
	
}
