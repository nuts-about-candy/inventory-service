package com.nutsaboutcandy.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.inventory.service.ProdClassificationService;

@RestController
@RequestMapping("/inventory/product-class")
public class ProductClassificationController {

	@Autowired
	private ProdClassificationService prodClassService;
	@GetMapping
	public ResponseEntity<Object> getAllProductClass() {
		
		return ResponseEntity.ok().body(prodClassService.getAllProdClassification());
	}
}
