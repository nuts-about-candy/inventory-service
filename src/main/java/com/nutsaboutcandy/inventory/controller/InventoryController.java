package com.nutsaboutcandy.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.inventory.entity.Product;
import com.nutsaboutcandy.inventory.model.ProductsWeights;
import com.nutsaboutcandy.inventory.service.InventoryService;

@RestController
@RequestMapping("/inventory")
public class InventoryController {
	
	@Autowired
	private InventoryService invService;

//	@PostMapping("/has-quantity")
//	public ResponseEntity<Object> hasQuantity(@RequestBody ProductsWeights weights) {
//		
//		try {
//			invService.hasQuantity(weights);
//		} catch(RuntimeException e) {
//			return ResponseEntity.badRequest().body(e.getMessage());
//		}
//		return ResponseEntity.ok().build();
//	}
	
	@PostMapping
	public ResponseEntity<Object> addProduct(@RequestBody Product product) {
		
		long id = invService.addProduct(product);
		product.setId(id);
		
		return ResponseEntity.ok().body(product);
	}
	
	@PutMapping
	public ResponseEntity<Object> editProduct(@RequestBody Product product) {
		
		invService.editProduct(product);
		
		return ResponseEntity.ok().build();
	}
	
//	@GetMapping
//	public ResponseEntity<Object> getAllProducts() {
//		
//		return ResponseEntity.ok().body(invService.getAllProducts());
//	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getAllProductsByProductId(@PathVariable("id") long id) {
		
		return ResponseEntity.ok().body(invService.getProductByProductId(id));
	}
	
	@GetMapping("/category/{id}")
	public ResponseEntity<Object> getAllProductsByCategoryId(@PathVariable("id") long id) {
		
		return ResponseEntity.ok().body(invService.getProductsByCategoryId(id));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") long id) {
		
		invService.deleteProduct(id);
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/reset")
	public ResponseEntity<Object> resetStocks() {
		
		invService.resetStocks();
		
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/add")
	public ResponseEntity<Object> add1kgToStocks() {
		
		invService.add1kgToStocks();
		
		return ResponseEntity.ok().build();
	}
	
}
