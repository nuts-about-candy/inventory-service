FROM openjdk:8-jdk-alpine

ADD target/inventory-service-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8002

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "app.jar"]

# Maven
# mvn clean package -Dspring.profiles.active=docker -Dmaven.test.skip=true
# Build
# docker build -t nuts-inventory .